# conslablib

Contains the a library for some tools for the conslab projects.

Especially contains python-drivers for the TriggerBox and the StimulationBox.

Also it contains some Tools for tkinter
(ToolTips, MouseScroll Support, Nice looking Linux-Theme)
