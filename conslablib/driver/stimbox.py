import serial
from serial.tools.list_ports import comports
import threading
import weakref
import copy
import time

from crccheck.crc import CrcXmodem as CRC16

HEADER = b'\xAA\xAA'

ID_INIT = b'\x10'
ID_AMPL = b'\x11'
ID_LENG = b'\x12'
ID_OUTP = b'\x20'

ID_FW = b'\x30'

RET_IMP = b'\x40'
RET_ERR = b'\x60'

ERROR_CODES = {
    0x00: "OK",
    0x01: "Software error",
    0x10: "Received corrupt message",
    0x11: "CRC error",
    0x12: "Invalid values in config message",
    0x13: "Invalid values in control message",
    0x14: "Buffer full"
}


def _create_channel_data(channel, A, B, ampl, f, pw, dur):
    """
    Create the data for one channel pair

    Params:
    -------
        channel: int8
            Channel to configure
        A: int8
            First electrode number
        B: int8
            Second electrode number
        ampl: int16
            Amplitude in 1/10 mA
        f: int16
            Frequency in Hz
        pw: int16
            Pulsewidth in ms
        dur: int16
            Stimulus duration in ms
    """
    data = channel.to_bytes(1, 'big')
    data += A.to_bytes(1, 'big')
    data += B.to_bytes(1, 'big')
    data += ampl.to_bytes(2, 'big')
    data += f.to_bytes(2, 'big')
    data += pw.to_bytes(2, 'big')
    data += dur.to_bytes(2, 'big')
    return data


class DummySerial():
    def __init__(self):
        self.msg = b'\xaa\xaa\x05\x60\x00'
        self.msg += CRC16.calc(self.msg).to_bytes(2, 'big')
        self.i = 0
        self._is_open = False
        self.port = None

    def write(self, msg):
        threading.Thread(target=print, args=(msg, )).start()

    def close(self):
        self._is_open = False
        pass

    def open(self):
        self._is_open = True
        pass

    def is_open(self):
        return self._is_open

    def read(self, len_):
        out = self.msg[self.i:self.i+len_]
        self.i = (self.i + len_) % len(self.msg)
        return out

    def flush(self):
        self.i = 0


def receive_data_thread(ser, event, print_ok):
    while(event.is_set()):
        try:
            if ser.port is None:
                time.sleep(1)
            msg = read_msg(ser)
            if msg is None:
                continue
            print_message(msg, print_ok)
        except serial.SerialException as e:
            print(repr(e))
            ser.flush()


def read_msg(ser):
    msg = ser.read(3)
    if msg == b'':
        return None
    if msg[0:2] == b'\xaa\xaa':
        len_ = msg[-1]
    else:
        raise serial.SerialException("Wrong Header")
    msg += ser.read(len_-3)
    crc = ser.read(2)
    if not CRC16.calc(msg) == int.from_bytes(crc, 'big'):
        print(msg, crc)
        raise serial.SerialException("Return message CRC is wrong")
    return msg


def print_impedance_message(msg):
    vals = msg.split(b"  ")
    _, vals[0] = vals[0].split(b"\x00", 1)
    out = "Impedance for channel {} = {}\u03A9\n"
    vals = vals[:-1]
    msgs = ""
    for val in vals:
        ch, value = val.split(b" ")
        msgs += out.format(ch[0],int(value))
    print(msgs)

    # print(msg)
    # print("Impedance between {} and {} = {}\u03A9".format(
    #     msg[4], msg[5], int.from_bytes(msg[6:8], 'big')
    # ))


def print_error_message(msg, print_ok):
    err_code = msg[4]
    if err_code == 0x00 and not print_ok.is_set():
        return
    print(ERROR_CODES[err_code])


def print_message(msg, print_ok):
    if msg[3:4] == RET_IMP:
        print_impedance_message(msg)
    elif msg[3:4] == RET_ERR:
        print_error_message(msg, print_ok)


def _shutdown_stimbox(thread, ser, event):
    event.clear()
    try:
        thread.join()
        print("Receiver Thread stopped.")
    except Exception:
        pass
    try:
        ser.close()
        print("Serial connection closed.")
    except Exception:
        pass


class TIISR():
    def __init__(self, print_ok=False):
        self._ser = serial.Serial(
            baudrate=115200,
            parity=serial.PARITY_EVEN,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            timeout=3
        )
        self.event = threading.Event()
        self._print_ok = threading.Event()
        self.set_print_ok(print_ok)
        self.ser = self._ser
        self.dummy = False
        self.id_str = None

    def _create_message(self, type_, data_):
        msg = HEADER
        msg += (len(data_)+4).to_bytes(1, 'big')
        msg += type_
        msg += data_
        msg += CRC16.calc(msg).to_bytes(2, 'big')
        msg += b'\xac\xad'
        return msg

    def _send_message(self, msg):
        try:
            self.ser.write(msg)
        except serial.SerialException as e:
            print(repr(e))

    def _find_port(self):
        fw_msg = self._create_message(ID_FW, b'')
        for port in comports():
            ser = copy.copy(self._ser)
            try:
                dev = port.device
            except AttributeError:
                dev = port[0]
            ser.port = dev
            try:
                with ser:
                    print("Try", dev)
                    ser.flushInput()
                    ser.write(fw_msg)
                    while True:
                        msg = read_msg(ser)
                        if msg is None:
                            raise serial.SerialException("No Message received")
                        if msg[3:4] == ID_FW:
                            break
                nr = msg[4]
                msg = msg[5:].decode("utf-8")
                if msg.find("TIISR") >= 0:
                    self.id_str = (nr, msg)
                    print("ID", self.id_str)
                return dev
            except serial.SerialException as e:
                repr(e)
                continue
        return None

    def is_open(self):
        return self.ser.is_open

    def open(self, port=None):
        if self.is_open():
            return
        if port is None:
            port = self._find_port()
        try:
            assert port is not None, "Port cannot be None"
            print(self.__class__.__name__, "on port", port)
            self.ser = copy.copy(self._ser)
            self.ser.port = port
            self.ser.open()
            self.dummy = False
        except Exception as e:
            print(repr(e), "Use Dummy Mode")
            self.ser = DummySerial()
            self.ser.open()
            self.dummy = True

        self.event.set()
        self.recv_thread = threading.Thread(
           target=receive_data_thread, args=(
               self.ser, self.event, self._print_ok
            ), daemon=True
        )
        self.recv_thread.start()
        self._finalizer = weakref.finalize(
            self, _shutdown_stimbox,
            self.recv_thread, self.ser, self.event
        )

    def close(self):
        self._finalizer()

    def initialize(self, state=1):
        """
        Initialize the device.

        Params:
        -------

        state: int8
            The state in which to put the device in. There are 3 states:
                0 = Control device via Visu-Program
                1 = Control device from external program (default)
                2 = Do cyclic movements that where prepared
        """
        msg = self._create_message(ID_INIT, state.to_bytes(1, 'big'))
        self._send_message(msg)

    def max_amp_diff(self, threshold=None):
        """
        Amplitude monitoring

        Params:
        -------

        threshold: int16 or None
            Set the max allowed amplitude difference in 1/10 mA.
            None = disable amplitude monitoring
        """
        if threshold is None:
            data = b'\x00\x00\x00'
        else:
            data = b'\x01'+threshold.to_bytes(2, 'big')
        msg = self._create_message(ID_AMPL, data)
        self._send_message(msg)

    def default_stim_duration(self, duration):
        """
        The default stimulus duration.
        Used if duration is 0 in channel configuration.

        Params:
        -------

        duration: int16
            The default duration in ms. If duration is 0 here and in
            control message no stimulus is performed.
        """
        data = duration.to_bytes(2, 'big')
        msg = self._create_message(ID_LENG, data)
        self._send_message(msg)

    def control_channels(
        self, channel, A, B,
        amplitude, pulsewidth, frequency, duration,
        *args
    ):
        """
        Control one channel pair

        Params:
        -------

        A: int8
            First channel number
        B: int8
            Second channel number
        amplitude: int16
            The amplitude in 1/10 mA
        frequency: int16
            The frequency in Hz
        pulsewidth: int16
            The pulsewidth in ms
        duration: int16
            The stimulus duration in ms
        *args:
            More control messages (same sequence as above)
            Be cautious with this! If length is wrong, nothing
            is sent to the device.
        """
        data = _create_channel_data(
            channel, A, B, amplitude, frequency, pulsewidth, duration
        )
        if(len(args) % 7 != 0):
            raise IndexError("Args has wrong size")
        for i in range(0, len(args), 7):
            data += _create_channel_data(*args[i:i+7])

        msg = self._create_message(ID_OUTP, data)
        self._send_message(msg)

    def get_print_ok(self):
        return self._print_ok.is_set()

    def set_print_ok(self, val):
        if val is True:
            self._print_ok.set()
        else:
            self._print_ok.clear()

    print_ok = property(
        get_print_ok, set_print_ok,
        doc="Whether OK return messages should be printed."
    )

    @property
    def port(self):
        try:
            return self.ser.port
        except Exception:
            return None


if __name__ == "__main__":
    stim = TIISR()
    stim.print_ok = True
    try:
        stim.open()
        stim.initialize()
        stim.max_amp_diff()
        stim.max_amp_diff(300)
        stim.default_stim_duration(0)
        stim.default_stim_duration(300)
        stim.control_channels(
            0,1, 2, 30, 50, 60, 300,
        )
        stim.control_channels(
            0, 1, 2, 100, 200, 200, 300,
            1, 2, 3, 4, 5, 6, 7,
            2, 8, 9, 10, 11, 12, 13
        )
        try:
            stim.control_channels(
                0, 1, 2, 30, 50, 60, 300,
                1, 2, 3, 4, 5, 6
            )
        except IndexError:
            print("Too Short")
        time.sleep(4)
    finally:
        stim.close()
