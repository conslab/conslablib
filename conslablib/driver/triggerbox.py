import time
import weakref

from PyMata.pymata import PyMata
from PyMata.pymata import PyMata as Constants
from serial.tools.list_ports import comports
import serial


LCD_WRITE = 0x0A
LCD_CLEAR = 0x0B
LCD_SETCURSOR = 0x0C


def _shutdown_box(controller):
    try:
        if hasattr(controller, "core"):
            ser = controller.core.serial_port.my_serial
        else:
            ser = controller.transport.arduino
        if ser.isOpen():
            ser.close()
        print("Connection closed")
    except Exception as e:
        print(repr(e))


def _find_port(
    firmata_versions=["2.5"],
    ino_file="ConslabTriggerboxFirmata_{}.ino", 
    file_versions=["V1"]
):
    ino_files = [ino_file.format(v) for v in file_versions]
    for port in comports():
        ser = None
        try:
            dev = port.device
        except AttributeError:
            dev = port[0]
        try:
            print("Try", dev)
            ser = serial.Serial(dev, baudrate=57600, timeout=4)

            # Firmata sends firmware version automatically
            res = ser.read(128)
            # Byte 0 = REPORT_VERSION
            # Byte 1 = Major Version (2)
            # Byte 2 = Minor Version (5)
            # Byte 3 = SYSEX_START
            # Byte 4 = REPORT_FIRMWARE
            # Bytes 5 ... end-1 = current firmware
            # Byte end = SYSEX_END

            if len(res) > 7:
                version = "{}.{}".format(res[5], res[6])
                name = ""
                for i in range(7, len(res)-1, 2):
                    name += chr(int.from_bytes(res[i:i+2], 'little'))
                if version in firmata_versions and name in ino_files:
                    return dev
        except serial.SerialException as e:
            print(e)
            continue
        finally:
            if ser is not None and ser.isOpen():
                ser.close()


class DummyController():
    def __init__(self, *args):
        self._command_handler = self
        self.transport = self
        self.arduino = self
        self.open = True

    def send_sysex(self, *args):
        pass

    def isOpen(self):
        return self.open

    def close(self):
        self.open = False

    def open(self):
        self.open = True

    def digital_write(self, *args):
        pass

    def play_tone(self, *args):
        pass


class TriggerBox():

    def __init__(self, triggertime=0.011):
        self._sleep = time.sleep
        self.signal = 7
        self.buz2 = 6
        self.led_r = 8
        self.led_g = 10
        self.led_b = 9
        self.trigger = 14  # A0
        self.triggertime = triggertime
        self.led_map = {"r": self.led_r, "b": self.led_b, "g": self.led_g}
        self._finalizer = lambda: None
        self.controller = None
        self.dummy = False

    def is_open(self):
        return self.controller is not None

    def open(self, port=None):
        if self.is_open():
            return
        if port is None:
            port = _find_port()
        try:
            assert port is not None, "Port cannot be None"
            print("Using port", port)
            self.controller = PyMata(port)
            self.controller.set_pin_mode(
                self.trigger, Constants.OUTPUT, Constants.DIGITAL
            )
            self.dummy = False
        except Exception as e:
            print(repr(e))
            self.controller = DummyController()
            self.dummy = True
        self._finalizer = weakref.finalize(
            self, _shutdown_box, self.controller
        )

    def close(self):
        self._finalizer()
        self.controller = None

    # # features of the TriggerBox # #

    def sleep_trigger(self, time, trig=False):
        ttime = 0
        if trig:
            ttime = self.triggertime
            self.controller.digital_write(self.trigger, 1)
            self._sleep(self.triggertime)
            self.controller.digital_write(self.trigger, 0)
        if time > ttime:
            self._sleep(time - ttime)

    def red_led(self, time, trig=False):
        self.controller.digital_write(self.led_r, 1)
        self.sleep_trigger(time, trig)
        self.controller.digital_write(self.led_r, 0)

    def green_led(self, time, trig=False):
        self.controller.digital_write(self.led_g, 1)
        self.sleep_trigger(time, trig)
        self.controller.digital_write(self.led_g, 0)

    def blue_led(self, time, trig=False):
        self.controller.digital_write(self.led_b, 1)
        self.sleep_trigger(time, trig)
        self.controller.digital_write(self.led_b, 0)

    def play_signal(self, time, trig=False):
        self.controller.digital_write(self.signal, 1)
        self.sleep_trigger(time, trig)
        self.controller.digital_write(self.signal, 0)

    def play_tone(self, freq, time=-1, trig=False):
        """
        Play a tone with a given frequence.
        If time is 0 the current tone is stopped.
        If time is < 0 the tone plays forever.
        """
        ttype = Constants.TONE_TONE
        if time == 0:
            ttype = Constants.TONE_NO_TONE
            time = -1
        elif time < 0:
            time = 0
        self.sleep_trigger(0, trig)
        self.controller.play_tone(self.buz2, ttype, freq, int(time*1000))

    def lcd_write(self, text):
        data = []
        for c in text:
            data.append(ord(c))
        self.controller._command_handler.send_sysex(LCD_WRITE, data)

    def lcd_clear(self):
        self.controller._command_handler.send_sysex(LCD_CLEAR)

    def lcd_setcursor(self, col, row):
        data = [col, row]
        self.controller._command_handler.send_sysex(LCD_SETCURSOR, data)

    @property
    def port(self):
        try:
            return self.controller.transport.port_id
        except Exception:
            return None


if __name__ == "__main__":
    box = TriggerBox()
    box.open("COM1")
    box.close()
