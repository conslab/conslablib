""" The Base for the modules

Contains:
    - base class for the modules, that are
      connected to a driver and run independently of the UI
    - a DummyDriver (useful for Software Modules)
    - a general CLI
    - a shutdown mechanism

Submodules can run as standalone cli and standalone UI version
if a file with same name is under the ./ui folder
Append following snippet to submodule to do so:

if __name__ == "__main__":
    if len(sys.argv) > 1:
        run_cli(<SubModuleClient>)
    else:
        from ui.<submodule> import run_ui, <SubModuleClientUI>
        run_ui(<SubModuleClassUI>)
"""
import asyncio
try:
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
except Exception:
    pass
import weakref
import argparse
import abc
import numbers
import inspect
import ast
from pathlib import Path
import time
import threading


import mqttools


def relativize_if_possible(first, second):
    """
    Relativizes first path to second path if possible

    Returns:
    --------
        "" if first is "",
        the relative path (if possible) or just first
    """
    if first == "":
        return ""
    try:
        return Path(first).resolve().relative_to(
            Path(second).parent.resolve()
        ).as_posix()
    except Exception:
        return str(Path(first).resolve().as_posix())


def _shutdown_box(box, broker):
    """
    Closes the connection to the driver and
    (if started) the broker
    """
    try:
        box.close()
        if broker is not None:
            broker.stop()
    except Exception as e:
        print(repr(e))


class DummyDriver():
    """
    The dummy driver. That just mocks a real driver.
    For use with Software Modules
    """
    def __init__(self, **kwargs):
        pass

    def open(self, comport):
        pass

    def close(self):
        pass


class ConslabBrokerClient(abc.ABC):
    """
    The main class for a module.

    Basically it starts a connection to a device via a driver
    and receives config and flow messages.

    The config messages are usually exposed directly to the device,
    while the flow messages are translated via a "function file" into
    config messages.

    The "function file" is also set via config messages, but not the file
    itself, but its contents.

    The function file should contain functions for each flow message that
    can be received and is therefore coupled to the protocol file.
    It can also contain an "other" function that is used to handle flow
    messages that are not handled in another way.

    Each instance contains an unique topic that is created after calling
    the start() method. This topic is used to send config messages only to
    this instance.

    The controller should have a unique register topic for each instance
    to register the unique topics.

    Parameters:
    -----------
        host : str
            The IP-Address of the broker
        port : int
            The port of the broker
        topic : str
            The topic_stub for the config messages
            (Should be set from the subclass)
        driverClass : driver
            The class of the driver to use
            (Should be set from the subclass)
        debug : bool
            Whether debug messages should be print or not

    Classattribute:
        TERMINATE : bytes
            The message to terminate the client, must be send
            to shutdown the client.
    """
    TERMINATE = "TERMINATE".encode("utf-8")

    def __init__(self, host, port, topic, driverClass, **kwargs):
        self.topic = topic
        self.cfg_funcs = set()
        self.debug = False
        if "debug" in kwargs:
            self.debug = kwargs["debug"]

        self.log_file = None

        self.client = mqttools.Client(
            host, port, connect_delays=[]
        )
        try:
            asyncio.run(self.test_client())
            self.broker = None
        except Exception:
            self.broker = mqttools.BrokerThread((host, port))
            self.broker.start()
        self.running = False
        self.time = 0
        self.box = driverClass()
        self._finalize = weakref.finalize(
            self, _shutdown_box, self.box, self.broker
        )

    async def test_client(self):
        """
        Test whether the broker is available
        """
        await self.client.start()
        await self.client.stop()

    async def start(self, comport, id_):
        """
        Starts the connection to the driver, sends a message containing
        the unique topic on the unique register topic.

        If connection to the driver fails it sends also an error.

        After that it waits for messages and handles them if they arrive.
        """
        topic_ = self.topic + str(id_)
        reg_topic = "/register_back" + str(id_)

        await self.client.start()
        await self.client.subscribe(topic_)
        await self.client.subscribe("/flow")
        await self.client.subscribe(reg_topic)

        self.box.open(comport)
        error = self.box.port
        if hasattr(self.box, "dummy") and self.box.dummy:
            error = 1

        self.running = True
        self.time = time.perf_counter()

        while(self.running):
            if reg_topic is not None:
                if self.debug:
                    print("Send topic", topic_, error)
                self.client.publish(
                    "/register{}".format(id_),
                    "{},{}".format(topic_, error).encode("utf-8")
                )

            topic, message = await self.client.messages.get()

            if self.debug:
                print(f'{topic_}: Got {message} on {topic}.')

            if topic == reg_topic:
                if message == b"ACK":
                    await self.client.unsubscribe(reg_topic)
                    reg_topic = None

            if message == self.__class__.TERMINATE:
                await self.handle_terminate(topic)
                break

            if topic == topic_:
                await self.handle_config_message(message)

            if topic == "/flow":
                await self.handle_flow_message(message)

            if topic is None:
                print(f'{topic_} connection lost.')
                break
        await self.client.stop()

    def write_to_file_thread(self, time, message):
        with open(self.log_file, "a") as of:
            of.write("{};{}\n".format(
                time - self.start_time, message.decode("utf-8")
            ))

    def write_to_file(self, message):
        if self.log_file is not None:
            threading.Thread(target=self.write_to_file_thread, args=(
                int(time.perf_counter()*1000), message
            )).start()

    async def handle_terminate(self, topic):
        """
        Handles the shutdown message
        (should be overidden for special cleanup)
        """
        self.write_to_file(self.__class__.TERMINATE)
        self.running = False
        return False

    @abc.abstractmethod
    async def handle_config_message(self, message):
        """
        Handles config messages
        if message started with CFG, the bytestream
        is executed to register the contained functions
        beware only async functions are allowed

        Parameters:
            message: bytes
                the message to handle

        Returns:
        --------
            True if message is handled, False otherwise
        """
        try:
            self.sleep(float(message))
            return True
        except ValueError:
            pass
        self.write_to_file(message)
        if message == b'TIME':
            self.print_time_delta()
            return True
        if message[0:4] == b'CFG,':
            self.clear_cfg_funcs()
            stmts = []
            # Only allow AsyncFunctionDefs and not evaluate the whole file
            for stmt in ast.parse(message[4:]).body:
                if isinstance(stmt, ast.AsyncFunctionDef):
                    stmts.append(stmt)
            try:
                exec(compile(ast.Module(stmts), "<string>", "exec"))
            except Exception:
                exec(compile(ast.Module(stmts, []), "<string>", "exec"))
            for key, value in locals().items():
                if inspect.isfunction(value):
                    self.cfg_funcs.add(key)
                    bound_method = value.__get__(self, self.__class__)
                    setattr(self, key, bound_method)
            return True
        if message[0:4] == b'LOG,':
            self.log_file = message[4:].decode("utf-8")
            if self.log_file == "":
                self.log_file = None
            else:
                try:
                    with open(self.log_file, "w") as of:
                        t = time.time()
                        self.start_time = int(time.perf_counter()*1000)
                        of.write("Created on: {}.{}\n".format(
                            time.strftime(
                                "%d.%m.%Y %H:%M:%S",
                                time.localtime(t)
                            ), str(t*1000)[-3:]
                        ))
                        of.write("timestamp in ms;message\n")
                    self.write_to_file_thread(
                        int(time.perf_counter()*1000), message
                    )
                except Exception as e:
                    print(repr(e))
                    self.log_file = None
            return True
        return False

    def clear_cfg_funcs(self):
        """
        Remove the added functions
        """
        for key in self.cfg_funcs:
            delattr(self, key)
        self.cfg_funcs = set()

    async def handle_flow_message(self, message):
        """
        Handle the flow messages. If the module has the function
        it is called. If not "other" is called if present.

        Returns:
        --------
            True if message is handled, False otherwise
        """
        self.write_to_file(message)
        message = message.decode("utf-8")
        try:
            if hasattr(self, message):
                msgs = await getattr(self, message)()
            elif hasattr(self, "other"):
                msgs = await getattr(self, "other")(message)
            else:
                return False
            if msgs is not None:
                for msg in msgs:
                    await self.handle_config_message(msg)
        except Exception as e:
            print(repr(e))
            return False
        return True

    def print_time_delta(self):
        print("{} Time: {:.2f}ms".format(
            u"\u0394", (time.perf_counter() - self.time)*1000
        ))
        self.time = time.perf_counter()

    def _accurate_sleep(self, t):
        ct = time.perf_counter() + t
        while time.perf_counter() < ct:
            pass

    def sleep(self, t):
        if t < 1:
            if t >= 0.002:
                t -= 0.001
            self._accurate_sleep(t)
        else:
            time.sleep(t)


def run_cli(clientClass, *args):
    """
    The Command Line Interface for the modules.
    See source for more information.

    Parameters:
    -----------
        clientClass : ConslabBrokerClient
            The actual implementation of the interface
    """
    async def publish(publisher, topic, msgs):
        """
        Publish the given messages on the topic
        (On WAIT execution waits amount of time in seconds)
        sends TERMINATE in the end.
        """
        await publisher.start()
        await asyncio.sleep(1)
        for msg in msgs:
            if msg.startswith("WAIT"):
                msg = msg.split("=")[1]
            msg = msg.encode("utf-8")
            publisher.publish(topic, msg)
        publisher.publish(topic, ConslabBrokerClient.TERMINATE)
        await publisher.stop()

    async def combine(publisher, client, args):
        """
        Starts the sending the given messages from the command line
        """
        id_ = 0
        msgs = []
        for msg in args["message"]:
            if msg.startswith("ID="):
                id_ = msg.split("=")[1]
            else:
                msgs.append(msg)
        await asyncio.gather(
            client.start(args["comport"], id_),
            publish(publisher, client.topic+str(id_), msgs)
        )

    parser = argparse.ArgumentParser(
        description="CLI to control the {}.".format(clientClass.__name__)
    )
    parser.add_argument(
        "-c", "--comport",
        help="The serial port for the {}, \
            if not given it will be detected.".format(clientClass.__name__)
    )
    parser.add_argument(
        "-b", "--broker_host",
        default="localhost",
        help="The Host of the message broker. Default: 'localhost'"
    )
    parser.add_argument(
        "-p", "--broker_port",
        default=1883,
        help="The Port of the message broker. Default: 1883"
    )
    parser.add_argument(
        "-d", "--debug",
        action="store_true",
        help="If set verbose debug messages are printed."
    )
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument(
        "-m", "--message",
        nargs="+",
        help="Messages to send to the {} and \
            terminate afterwards.".format(clientClass.__name__)
    )
    group.add_argument(
        "-r", "--run",
        metavar="ID",
        help="Run as a process and wait for TERMINATE message. \
            Must add an ID for the broker."
    )

    args = vars(parser.parse_args(args=args[1:]))

    host = args["broker_host"]
    port = int(args["broker_port"])

    boxClient = clientClass(host, port, debug=args["debug"])

    publisher = mqttools.Client(host, port)

    if args["run"]:
        asyncio.run(boxClient.start(args["comport"], args["run"]))
    else:
        asyncio.run(combine(publisher, boxClient, args))
