"""
The common UI Interface for the modules

This holds some common values, that need to be handled with care
see ConslabBrokerClientUI class documentation for details.

SubmoduleUIs can run as standalone GUIs but they load
their appropriate submodules.
"""
import tkinter.ttk as ttk
import tkinter as tk
from tkinter import messagebox

import weakref
import asyncio
try:
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
except Exception:
    pass
import threading
import multiprocessing as mp
import abc
import copy
from pathlib import Path
from itertools import count
import sys

from serial.tools.list_ports import comports
import mqttools

from conslablib.utils.tk import ToolTip, Dialog, init_conslab_tk_style
from conslablib.utils.tk.filedialog import askopenfile, asksaveasfilename
from conslablib.interfaces.brokerclient import ConslabBrokerClient, \
    relativize_if_possible


lock = threading.Lock()


def check_file(fname, binary={}):
    if fname in binary:
        return binary[fname]
    return fname


class BrokerConfDialog(Dialog):
    """
    Creates a dialog, where one can set the Broker Host and port

    Parameters:
    -----------
        ok_cancel : unknown
            if it is not set the cancel button is omitted

    Returns:
    --------
        True if ok is pressed, False if cancel is pressed
    """

    def body(self, master, **kwargs):
        """
        The body of the dialog

        Paramters:
        ----------
            ok_cancel : unknown
                if it is not set the cancel button is omitted
        """
        ttk.Label(master, text="Broker Host").grid(row=10, column=10)
        ttk.Label(master, text="Broker Port").grid(row=20, column=10)
        foc = ttk.Entry(
            master, textvariable=kwargs["host_var"]
        )
        foc.grid(row=10, column=20, sticky=tk.EW)
        ttk.Entry(
            master, textvariable=kwargs["port_var"]
        ).grid(row=20, column=20, sticky=tk.EW)
        master.columnconfigure(20, weight=1)

        self.btnOK.configure(text="OK")
        if not hasattr(kwargs, "ok_cancel"):
            self.btnC.destroy()
        self.result = False
        return foc

    def apply(self):
        self.result = True


def run_client(module, com, host, port, id, debug):
    """
    Process to run the Client
    """
    import sys
    import inspect
    for member in inspect.getmembers(module):
        print(member)
        if member[0] == "main":
            function = member[1]
            break

    args = [
        function.__qualname__,
        "-b", str(host), "-p", str(port), "-r", str(id)
    ]
    if debug:
        args.append("-d")
    if com is not None and com != "auto":
        args.extend(["-c", str(com)])

    sys.argv = args  # .extend(args)
    function()


def run_sync(process, func, lock, site_path):
    """
    To run the connect thread in order to
    prevent race conditions
    """
    lock.acquire()
    if site_path is not None:
        arg = "--site-path="+site_path.as_posix()
        found = None
        for i in range(len(sys.argv)):
            if sys.argv[i].startswith("--site-path"):
                found = i
                break
        if found is None:
            sys.argv.append(arg)
        else:
            sys.argv[found] = arg
    process.start()
    asyncio.run(func())
    lock.release()


async def publish_message(publisher, topic, message):
    """
    To publish a message on topic
    """
    await publisher.start()
    publisher.publish(topic, message)
    await publisher.stop()


def _shutdown(publisher, process, topic):
    """
    Securely shutdown all processes, threads and components
    """
    print(topic + " clean up")
    while(process.is_alive()):
        asyncio.run(
            publish_message(
                publisher, topic, ConslabBrokerClient.TERMINATE)
        )
        process.join(1)
    print("Process finished")


class ConslabBrokerClientUI(ttk.Frame, abc.ABC):
    """
    The common UI for the modules, that can be loaded into the TriggeBox App.
    Each instance contains an unique ID, which is also used as id and topic
    to register the clients itself

    Attributes:
    -----------
        send_on_connect : dict
            Contains the variables and values that are send on connect and
            (currently) immediatly while connected. There is a function
            _send_on_connect(var) where you can register a variable for
            this dictionary
        id : int
            The unique ID for each instance

    Parameters:
    -----------
        w : tk.Widget
            the root widget
        title : str
            title + id is shown as header
        program : path
            path to the client program (which is executed as subprocess)
    """
    ids_ = count(0)

    @abc.abstractmethod
    def __init__(self, w, title, **kwargs):
        super().__init__(w)
        self.id = next(self.__class__.ids_)
        self.program = "{}.{}".format(title.lower(), title.lower())
        if "program" in kwargs:
            self.program = kwargs["program"]
        self.pyz_file = None
        if "pyz_file" in kwargs:
            self.pyz_file = kwargs["pyz_file"]
        self.site_path = None
        if "site_path" in kwargs:
            self.site_path = kwargs["site_path"]
        self.debug = False
        if "debug" in kwargs:
            self.debug = True

        self.title = title + str(self.id)

        self.available_ports_start = {"auto": "auto", "DUMMY MODE": "dummy"}
        self.available_ports = copy.copy(self.available_ports_start)
        self.cfg_funcs = {}
        self.cfg = None
        self.send_on_connect = {}
        self.port = ""
        self.func_file = ""

        self.topic = ""
        self.client = None
        self.alive = True  # Used to detect a close event while connecting
        self.connected = False

        self.host_var = tk.StringVar()
        self.host_var.set("localhost")
        self.port_var = tk.IntVar()
        self.port_var.set(1883)

        self.PORT_VAR = tk.StringVar()
        self.PORT_VAR.trace("w", self._set_port)
        self.PORT_VAR.set("auto")

        # The widgets that are disabled when connect is pressed
        self.flip_widgets = []

        self.headGrid = ttk.Frame(self)
        self.headGrid.pack(side=tk.TOP, fill=tk.X)

        self.port_box = ttk.Combobox(
            self.headGrid, state="readonly",
            textvariable=self.PORT_VAR
        )
        self.port_box.pack(side=tk.LEFT, expand=tk.TRUE, fill=tk.X, padx=2)

        self.check_available_ports()

        self.brokerconf_btn = ttk.Button(
            self.headGrid, text="\u2699", width=3,
            command=self.broker_conf
        )
        self.brokerconf_btn.pack(side=tk.RIGHT, fill=tk.X)

        self.connect_btn = ttk.Button(
            self.headGrid, text="  Connect  ", command=self._connect
        )
        self.connect_btn.pack(side=tk.RIGHT, fill=tk.X)

        ToolTip(self.brokerconf_btn, text="Message Broker Configuration")

        ## LOG FILE

        frame = ttk.Frame(self)
        frame.pack(side=tk.TOP, fill=tk.X)
        self.log_var = tk.StringVar()
        self.log_var.trace("w", self.log_trace)

        ttk.Label(frame, text="Log-File").pack(side=tk.LEFT, fill=tk.X)
        self.log_entry = ttk.Entry(
            frame, textvariable=self.log_var, state=tk.READABLE
        )
        self.log_entry.pack(side=tk.LEFT, fill=tk.X, expand=tk.TRUE)

        btn = ttk.Button(
            frame, text="O", width=3,
            command=self.log_open
        )
        btn.pack(side=tk.LEFT, fill=tk.X)
        ToolTip(btn, "File to store the send messages.")

        # Load Protocol

        frame = ttk.Frame(self)
        frame.pack(side=tk.TOP, fill=tk.X)

        self.load_btn = ttk.Button(
            frame, text="Load Protocol Functions",
            command=self.load_protocol_funcs
        )
        self.load_btn.pack(side=tk.LEFT, expand=tk.TRUE, fill=tk.X, padx=3)

        self.func_name_f = ttk.Frame(frame)
        self.func_name_f.pack(side=tk.LEFT, fill=tk.X)

        self.func_name = ttk.Label(self.func_name_f)
        self.func_name.pack(side=tk.LEFT, fill=tk.BOTH)
        self.func_x = ttk.Label(
            self.func_name_f, text=u"\u2715", width=3,
            anchor=tk.CENTER
        )
        self.func_x.pack(side=tk.LEFT, fill=tk.BOTH)
        t = ToolTip(self.func_x, "Clear Protocol Functions")
        self.func_x.bind(
            "<Enter>",
            lambda event:
                [t.enter(event), self.func_x.configure(text=u"\u2716")]
        )
        self.func_x.bind(
            "<Leave>",
            lambda event:
                [t.leave(event), self.func_x.configure(text=u"\u2715")]
        )
        self.func_x.bind("<Button-1>", self.clear_protocol_cfg)

        self.func_name_f.pack_forget()

        self.bind("<Destroy>", self._destroy)

        ttk.Frame(self).pack(side=tk.BOTTOM, expand=tk.TRUE, fill=tk.BOTH)

    def _send_on_connect(self, key, val):
        """
        Add key value to send_on_connect

        ATTENTION: If name already exists it will be overidden.

        Parameters:
        -----------
            key : str
                The key to add the value to
            val : unknown
                The value to add
        """
        self.send_on_connect[key] = val
        if self.connected:
            asyncio.run(
                publish_message(self.publisher, self.topic, val)
            )

    def log_trace(self, *args):
        file0 = self.log_var.get()
        if file0:
            msg = "LOG,{}".format(file0).encode("utf-8")
            self._send_on_connect("log", msg)

    def log_open(self, file0=None):
        if file0 is None:
            initialfile=self.title + ".csv"
            file0 = asksaveasfilename(
                self, "Open file to store values",
                initialfile=initialfile,
                filetypes=[("Comma Seperated Value File", ".csv"), ("All Files", "*")]
            )
            if file0 and not Path(file0).suffix:
                file0 += ".csv"

        if file0:
            self.log_var.set(file0)
            self.log_entry.update_idletasks()
            self.log_entry.xview_moveto(1)

    def clear_protocol_cfg(self, event):
        """
        Clear the function implementations
        """
        self.func_name_f.pack_forget()
        self.cfg_funcs = {}
        self.cfg = b"CFG,"
        self.func_file = ""
        self._send_on_connect("CFG", self.cfg)

    def check_available_ports(self):
        """
        Check which comports are available
        """
        self.available_ports = copy.copy(self.available_ports_start)
        for i, port in enumerate(comports()):
            try:
                port_desc = port.device
                device = port.device
            except Exception:
                port_desc = port[0]
                device = port[0]
            try:
                port_desc += \
                    " | (" + port.description +  \
                    " " + port.manufacturer + ")"
            except Exception:
                pass
            self.available_ports[port_desc] = device
        tmp = list(self.available_ports.keys())
        tmp.append(tmp.pop(1))
        self.port_box.configure(values=tmp)

    def broker_conf(self):
        """
        Open the Broker config dialog
        """
        BrokerConfDialog(
            self, title="Broker Configuration",
            host_var=self.host_var, port_var=self.port_var
        )

    def _destroy(self, *args):
        """
        Called when the widget is destroyed.
        Used to cleanup nicely
        """
        self.alive = False
        self.__disconnect()

    def __disconnect(self, *args):
        """
        Used for cleanup
        """
        if hasattr(self, "_finalize"):
            self._finalize()

    def create_publisher(self):
        """
        Creates the publisher to publish messages to the client.
        """
        self.publisher = mqttools.Client(
            self.host_var.get(), self.port_var.get(), connect_delays=[]
        )

    async def await_topic(self):
        """
        This function is important and belongs to the connect function.
        It is used as callback to check when the connection to the device
        is finished.

        It starts am mqttools.Client that waits for the client to send
        their topic for configuration messages.

        The topic contains also an Error Message. If an error occurs the
        UI goes into Dummy Mode. And if UI is shutdown during wait it sends
        TERMINATE to the client when the topic arrives.

        After the topic is received the UI state is changed and the
        send_on_connect values are sent to the device.

        Subclasses can overide this to change additional UI states.
        """
        client = mqttools.Client(
            self.host_var.get(), self.port_var.get(),
            connect_delays=[1]
        )
        reg_topic = "/register" + str(self.id)
        await client.start()
        await client.subscribe(reg_topic)

        while True:
            try:
                topic, msg = await asyncio.wait_for(
                    client.messages.get(), timeout=0.5
                )
                break
            except asyncio.TimeoutError:
                client.publish("/register_back"+str(self.id), b"ERR")
        client.publish("/register_back"+str(self.id), b"ACK")
        await client.unsubscribe(reg_topic)

        if topic == reg_topic:
            top, err = msg.decode("utf-8").split(",")
            if self.alive:
                self.topic = top
                self._finalize = weakref.finalize(
                    self, _shutdown, **{
                        "publisher": self.publisher,
                        "process": self.process,
                        "topic": self.topic
                    }
                )
            else:
                client.publish(top, ConslabBrokerClient.TERMINATE)
            self.check_available_ports()
            if err == "1" or err == "None":
                messagebox.showwarning(
                    "Warning",
                    top + ": Wrong Port Configuration " +
                    self.port + ". Go into DUMMY MODE."
                )
                self.PORT_VAR.set("DUMMY MODE")
            else:
                # Set the port value into the ComboBox
                self.PORT_VAR.set(list(
                    self.available_ports.keys()
                )[list(self.available_ports.values()).index(err)])
        await client.stop()
        if self.alive:
            self.connected = True
            self.connect_btn.configure(state=tk.NORMAL)
            for elem in self.flip_widgets:
                elem.configure(state=tk.NORMAL)
            for _, vals in self.send_on_connect.items():
                await publish_message(self.publisher, self.topic, vals)

    def _connect(self, *args):
        """
        Connect to the client. Therefore the client is started via
        the CLI mode as subprocess but in a synchronized Thread to
        prevent multiple connects to different clients to interfere
        each other.
        """
        self.port_box.configure(state=tk.DISABLED)
        self.connect_btn.configure(
            text="Disconnect", command=self._disconnect,
            state=tk.DISABLED
        )
        self.brokerconf_btn.configure(state=tk.DISABLED)
        # start serial connection
        self.create_publisher()
        # subprocess_str = "python {} -b {} -p {} -r {} -d".format(
        #     self.program, self.host_var.get(), self.port_var.get(), self.id
        # )
        # if self.port is not None and self.port != "auto":
        #     subprocess_str += " -c {}".format(self.port)
        import inspect
        for member in inspect.getmembers(
            sys.modules[self.program], inspect.isfunction
        ):
            if member[0] == "main":
                func = member[1]
                break

        args = [
            func.__qualname__,
            "-b", self.host_var.get(),
            "-p", str(self.port_var.get()),
            "-r", str(self.id)
        ]
        if self.debug:
            args.append("-d")
        if self.port is not None and self.port != "auto":
            args.extend(["-c", str(self.port)])

        self.process = mp.Process(
            target=func, args=args
        )
        thread = threading.Thread(
            target=run_sync, args=(
                self.process, self.await_topic, lock, self.site_path
            )
        )
        thread.start()

    def _disconnect(self, *args):
        """
        Disconnects the client
        """
        self.check_available_ports()
        self.port_box.configure(state=tk.READABLE)
        self.connect_btn.configure(text="  Connect  ", command=self._connect)
        self.brokerconf_btn.configure(state=tk.NORMAL)
        for elem in self.flip_widgets:
            elem.configure(state=tk.DISABLED)
        self.__disconnect()
        self.connected = False

    def _set_port(self, *args):
        try:
            self.port = self.available_ports[self.PORT_VAR.get()]
        except Exception:
            self.port = None

    def load_protocol_funcs(self, file0=None):
        """
        Load the protocol function. Protocol functions are normal python
        functions but can have a tool information on the first lines as a
        comment. Therefore the functions are coupled to the module.

        It also seperates the functions and stores its contents into
        a dictionary to be able to show it into the MainApp.
        """
        if not file0:
            file0 = askopenfile(
                self, title="Protocol File to load",
                filetypes=[("Python file", ".py"), ("All Files", "*")],
            )
        if file0 and Path(file0).exists():
            cfg = {}
            func_text = {}
            with open(file0, "r") as of:
                header = True
                text = ""
                func_name = ""
                max_width = 0
                for line in of:
                    # Check the header comment
                    if header and line.startswith("#"):
                        split = line.split(":")
                        cfg[split[0][1:].strip()] = split[1].strip()
                    else:
                        header = False

                    # Get the text of the functions
                    if line.startswith("async def "):
                        if func_name:
                            func_text[func_name] = (text, max_width)
                            max_width = 0
                        func_name = line.split(
                            " ", maxsplit=2
                        )[2].split("(")[0].strip()
                        text = line + "\n"
                        if len(line) > max_width:
                            max_width = len(line)
                    elif func_name:
                        if line.strip():
                            if len(line) > max_width:
                                max_width = len(line)
                            text += line + "\n"
                if func_name:
                    func_text[func_name] = (text, max_width)
            if "tool" in cfg:
                if not self.title.startswith(cfg["tool"]):
                    messagebox.showerror(
                        "Error",
                        "Tool information does not correspond " +
                        "to module title."
                    )
                    return
            else:
                messagebox.showwarning(
                    "Warning",
                    file0 + "\nhas no tool information set." +
                    "\nThe functions may not work correctly."
                )
            self.func_file = file0
            self.func_name.configure(text=Path(file0).name)
            self.func_name_f.pack()
            self.cfg_funcs = func_text
            with open(file0, "rb") as of:
                self.cfg = b'CFG,' + of.read()
            self._send_on_connect("CFG", self.cfg)

    @abc.abstractmethod
    def get_setup_dict(self, start_path):
        """
        Gets the dictionary that can be saved in setup files for this object.
        This implementation creates a header containing the config messages,
        the program name and the function file that is maybe loaded.

        Parameters:
        -----------
            start_path : path
                To relativize paths to

        Returns:
        --------
            The resulting dictionary and a list of file paths
        """
        prog_file = relativize_if_possible(self.program, start_path)
        if self.pyz_file is not None:
            prog_file = relativize_if_possible(self.pyz_file, start_path)
        dict0 = {"module": {
            "program_file": prog_file,
            "cfg_msg": []
        }}
        for _, msg in self.send_on_connect.items():
            dict0["module"]["cfg_msg"].append(msg.decode("utf-8"))
        func_file = relativize_if_possible(self.func_file, start_path)
        dict0["func_file"] = func_file
        dict0["log_file"] = self.log_var.get()

        return dict0, [prog_file, func_file]

    @abc.abstractmethod
    def load_setup_dict(self, dict0, bins={}):
        """
        Load the setup dictionary for this object.
        This implementation just reads the function file

        Parameters:
        -----------
            The dictionary to read

        Returns:
        --------
            list with already used dictionary keys
        """
        func_file = dict0["func_file"]
        if func_file:
            self.load_protocol_funcs(check_file(func_file, bins))
        self.log_var.set(dict0["log_file"])
        return ["module", "func_file", "log_file"]


class DummyClientUI(ConslabBrokerClientUI):
    def __init__(self, w, title, **kwargs):
        super().__init__(w, title, **kwargs)

    def get_setup_dict(self, start_path):
        return super().get_setup_dict(start_path)

    def load_setup_dict(self, dict0):
        return super().load_setup_dict(dict0)


class ConslabBrokerSoftwareClientUI(ConslabBrokerClientUI):
    """
    The implementation for a Software ClientUI as this does not
    need an external device. Removes also the port ComboBox
    """

    @abc.abstractmethod
    def __init__(self, w, title, **kwargs):
        super().__init__(w, title, **kwargs)
        self.port_box.pack_forget()
        ttk.Label(
            self.headGrid, text="SoftwareDevice"
        ).pack(side=tk.LEFT, expand=tk.TRUE, fill=tk.X)


def run_ui(clientUIclass):
    """
    Function to run the UI standalone

    Parameters:
    -----------
        clientUIclass : ConslabBrokerClientUI
            The concrete implementation of the UI
    """
    root = tk.Tk()
    root.style = init_conslab_tk_style(root)

    root.resizable(False, False)
    app = clientUIclass(root)
    app.pack(expand=tk.TRUE, fill=tk.BOTH, padx=3, pady=3)
    root.title(app.title)
    try:
        root.mainloop()
    except KeyboardInterrupt:
        app._destroy()
