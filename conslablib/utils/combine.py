import copy
import tempfile
import warnings
import numbers

from .scipy_signal import butter, sosfiltfilt
import numpy as np

try:
    import scipy.io as mat4py
    use_scipy = True
except ImportError:
    import mat4py
    use_scipy = False

special_keys = ["classes", "params", "emg", "emg_desc", "sequence"]
y_opts = ["Combine Same", "Add", "Reduce to New"]  # 0,1,2


def convert_to_numpy(dict_):
    for key in dict_:
        if isinstance(dict_[key], list):
            with warnings.catch_warnings():
                warnings.filterwarnings('error')
                try:
                    dict_[key] = np.array(dict_[key])
                except (Warning, Exception):
                    pass
        elif isinstance(dict_[key], dict):
            convert_to_numpy(dict_[key])
        elif isinstance(dict_[key], (str, numbers.Number)):
            dict_[key] = np.array(dict_[key], ndmin=1)
    return dict_


def convert_to_list(dict_):
    for key in dict_:
        if isinstance(dict_[key], np.ndarray):
            dict_[key] = dict_[key].tolist()
        elif isinstance(dict_[key], dict):
            convert_to_list(dict_[key])
        elif isinstance(dict_[key], list):
            for i in range(len(dict_[key])):
                if isinstance(dict_[key][i], np.ndarray):
                    dict_[key][i] = dict_[key][i].tolist()
    return dict_


def _load_mat(file_or_dict):
    if isinstance(file_or_dict, dict):
        tmp = tempfile.SpooledTemporaryFile()
        mat4py.savemat(tmp, file_or_dict)
        dat = mat4py.loadmat(tmp)
        if "combined" in dat:
            file_ = dat["combined"]
        else:
            file_ = "<temporary>"
    else:
        file_ = str(file_or_dict)
        try:
            dat = mat4py.loadmat(file_)
        except Exception:
            print("Cannot load {}".format(file_))
            raise
    if not use_scipy:
        dat = convert_to_numpy(dat)
    return dat, file_


def combine_mats(
    file1, file2, y_opt=1, cls1="Left", cls2="Right",
    filter_eeg=True
):
    savedict = {}
    dat1, file1 = _load_mat(file1)
    dat2, file2 = _load_mat(file2)

    length_eeg = dat1["eeg"].shape[1]
    length_emg = None

    if filter_eeg:
        try:
            fs1 = dat1["fsampling"][0, 0]
            fs2 = dat2["fsampling"][0, 0]
        except Exception:
            fs1 = dat1["fsampling"]
            fs2 = dat2["fsampling"]
            if not isinstance(fs1, numbers.Number):
                fs1 = fs1[0]
            if not isinstance(fs2, numbers.Number):
                fs2 = fs2[0]
        sos1 = butter(8, 0.5, 'hp', fs=fs1, output='sos')
        sos2 = butter(8, 0.5, 'hp', fs=fs2, output='sos')
        dat1["eeg"] = sosfiltfilt(sos1, dat1["eeg"])
        dat2["eeg"] = sosfiltfilt(sos2, dat2["eeg"])

    # # read the params and create dictionaries
    try:
        key1 = dat1["params"].keys()
        key2 = dat2["params"].keys()
    except Exception:
        key1 = dat1["params"].dtype.names
        key2 = dat2["params"].dtype.names

    params1 = {}
    for key in key1:
        elem = dat1["params"][key]
        if elem.size > 0:
            if elem.shape == (1, 1):
                elem = elem[0, 0]
            if len(elem.shape) < 2:
                elem = np.expand_dims(elem, axis=1)
            params1[key] = elem.tolist()
    params2 = {}
    for key in key2:
        elem = dat2["params"][key] + length_eeg
        if elem.size > 0:
            if elem.shape == (1, 1):
                elem = elem[0, 0]
            if len(elem.shape) < 2:
                elem = np.expand_dims(elem, axis=1)
            params2[key] = elem.tolist()

    # # THE EMG stuff, same names get extended and other names are appended
    # # to the array and padded with zeros (either before or after)
    if "emg" in dat1 and "emg" in dat2:
        dat1["emg"] = np.array(dat1["emg"], ndmin=2)
        dat2["emg"] = np.array(dat2["emg"], ndmin=2)
        length_emg = dat1["emg"].shape[1]
        length_emg2 = dat2["emg"].shape[1]

        tmp_emg = [dat1["emg"][i] for i in range(dat1["emg"].shape[0])]
        try:
            new_emg_desc = [s.strip() for s in dat1["emg_desc"]]
        except Exception:
            new_emg_desc = list(dat1["emg"].keys())
        try:
            emg_desc2 = [s.strip() for s in dat2["emg_desc"]]
        except Exception:
            emg_desc2 = list(dat2["emg"].keys())
        for i, elem in enumerate(emg_desc2):
            if elem in new_emg_desc:
                idx = new_emg_desc.index(elem)
                tmp_emg[idx] = np.concatenate((tmp_emg[idx], dat2["emg"][i]))
            else:
                tmp = np.concatenate((np.zeros((length_emg,)), dat2["emg"][i]))
                tmp_emg.append(tmp)
                new_emg_desc.append(elem)
        for i, elem in enumerate(new_emg_desc):
            if elem not in emg_desc2:
                tmp_emg[i] = np.concatenate((
                    tmp_emg[i], np.zeros((length_emg2,))
                ))
        tmp_emg = np.array(tmp_emg)
        savedict["emg"] = tmp_emg
        savedict["emg_desc"] = new_emg_desc

    # # Combine the triggers
    # NP UNIQUE MAY BE A PROBLEM !!!!
    new_trig = []
    if len(dat1["trigger"].shape) < 2:
        dat1["trigger"] = np.array(dat1["trigger"], ndmin=2)
    if len(dat2["trigger"].shape) < 2:
        dat2["trigger"] = np.array(dat2["trigger"], ndmin=2)

    new_trig.append(np.unique(np.concatenate((
        dat1["trigger"][0], dat2["trigger"][0] + length_eeg
    ))))
    if dat1["trigger"].shape[0] > 1 and dat2["trigger"].shape[0] > 1:
        new_trig.append(np.unique(np.concatenate((
            dat1["trigger"][1], dat2["trigger"][1] + length_emg
        ))))
    savedict["trigger"] = np.array(new_trig)

    if y_opt == 0:
        new_clss = [
            clss.strip() for clss in dat1["classes"]
            if clss.strip() in params1
        ]
        new_seq = [clss.strip() for clss in dat1["sequence"]]
        new_seq.extend([clss.strip() for clss in dat2["sequence"]])
        new_params = copy.deepcopy(params1)
        for key in params2:
            if key in new_params:
                for i, elem in enumerate(new_params[key]):
                    elem.extend(params2[key][i])
            else:
                new_params[key] = params2[key]
                new_clss.append(key)
    elif y_opt == 1:
        new_clss = [
            cls1 + "_" + clss.strip() for clss in dat1["classes"]
            if clss.strip() in params1
        ]
        new_clss.extend([
                cls2 + "_" + clss.strip() for clss in dat2["classes"]
                if clss.strip() in params2
            ]
        )
        new_seq = [cls1 + "_" + clss.strip() for clss in dat1["sequence"]]
        new_seq.extend(
            [cls2 + "_" + clss.strip() for clss in dat2["sequence"]]
        )
        new_params = {}
        for key, v in params1.items():
            new_params[cls1 + "_" + key] = v
        for key, v in params2.items():
            new_params[cls2 + "_" + key] = v
    elif y_opt == 2:
        if cls1 == cls2:
            new_clss = [cls1]
            new_params = {cls1+"1": [], cls2+"2": []}
            params = {cls1+"1": params1, cls2+"2": params2}
        else:
            new_clss = [cls1, cls2]
            new_params = {cls1: [], cls2: []}
            params = {cls1: params1, cls2: params2}
        new_seq = [cls1 for _ in dat1["sequence"]]
        new_seq.extend([cls2 for _ in dat2["sequence"]])
        for cls_ in new_params:
            for _, v in params[cls_].items():
                for i, elem in enumerate(v):
                    if len(new_params[cls_]) <= i:
                        new_params[cls_].append(elem)
                    else:
                        new_params[cls_][i].extend(elem)
        if cls1 == cls2:
            tmp = new_params[cls1+"1"]
            for i in range(len(tmp)):
                tmp[i].extend(new_params[cls2+"2"][i])
            new_params = {cls1: tmp}

    savedict["classes"] = new_clss
    savedict["params"] = new_params
    savedict["sequence"] = new_seq
    savedict["combined"] = [file1, file2, [length_eeg]]
    savedict["combined_desc"] = ["file1", "file2", ["len_eeg_file1"]]
    if length_emg is not None:
        savedict["combined"][2].append(length_emg)
        savedict["combined_desc"][2].append("len_emg_file1")

    not_in_file1 = []
    not_in_file2 = []
    for key, v1 in dat1.items():
        if key in savedict or key.startswith("not_in") or (
            key.startswith("__") and key.endswith("__")
        ):
            continue
        if key in dat2:
            v2 = dat2[key]
            try:
                if len(v1.shape) == 1:
                    if v1.shape == v2.shape and np.all(v1 == v2):
                        tmp = v1
                    else:
                        tmp = v1.tolist()
                        tmp.extend(v2.tolist())
                    savedict[key] = tmp
                else:
                    savedict[key] = np.concatenate((v1, v2), axis=1)
            except AttributeError:
                if v1 == v2:
                    tmp = v1
                else:
                    tmp = [v1, v2]
                savedict[key] = tmp
        else:
            savedict[key] = v1
            not_in_file2.append(key)
    for key, v2 in dat2.items():
        if key in savedict or key.startswith("not_in") or (
            key.startswith("__") and key.endswith("__")
        ):
            continue
        else:
            savedict[key] = v2
            not_in_file1.append(key)
    if not_in_file1:
        savedict["not_in_file1"] = not_in_file1
    if not_in_file2:
        savedict["not_in_file2"] = not_in_file2
    if not use_scipy:
        return convert_to_list(savedict)
    return savedict
