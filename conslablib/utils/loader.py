import sys
import os
import io
from zipfile import ZipFile

import numpy as np
import ast


def get_chunk_dict(z, chunk_name):
    chunks = {}
    for name in z.namelist():
        base, _ = os.path.splitext(name)
        pos0 = base.find(chunk_name)
        if pos0 >= 0:
            pos0 += len(chunk_name)
            chunks[int(base[pos0:])] = name
    return chunks


def read_state_chunks(filename):
    res = {}
    with ZipFile(filename, "r") as z:
        chunks = get_chunk_dict(z, "state_header")
        header_file = chunks[sorted(chunks)[-1]]
        header = str(z.read(header_file), "utf-8").split(";")
        for head in header:
            res[head] = []

    with ZipFile(filename, "r") as z:
        chunks = get_chunk_dict(z, "state_chunk")
        for chunk in sorted(chunks):
            head, data = z.read(chunks[chunk]).split(b'\n', 1)
            head = str(head, "utf-8").split(";")
            pos0 = 0
            for i, name in enumerate(header):
                try:
                    prev, post = head[i].split(":")
                    post = int(post)
                    assert int(prev) > 0
                except Exception:
                    res[name].append(None)
                    continue

                if post > 0:
                    pos1 = data.find(b':', pos0)
                    idx = int(data[pos0:pos1])
                    assert idx == i, \
                        "Index in file should be same as index in header"
                    dat = data[pos1+1:pos1+1+post]
                    pos0 = pos1+2+post
                    try:
                        d = np.load(io.BytesIO(dat))
                    except Exception:
                        d = str(dat, "utf-8")
                        try:
                            d = ast.literal_eval(d)
                        except Exception:
                            pass
                    res[name].append(d)
                else:
                    res[name].append(res[name][-1])
    return res


def read_data_chunks(filename):
    description = None
    res = []
    with ZipFile(filename, "r") as z:
        chunks = get_chunk_dict(z, "description")
        for chunk in sorted(chunks):
            description = np.load(io.BytesIO(z.read(chunks[chunk])))
        comb_axis = np.where(np.array(description.shape)==1)[0][0]
        chunks = get_chunk_dict(z, "data_chunk")

        for chunk in sorted(chunks):
            array = np.load(io.BytesIO(z.read(chunks[chunk])))
            res.append(array)
        res = np.concatenate(res, axis=comb_axis)
        chunks = get_chunk_dict(z, "flowcfg")
        for chunk in sorted(chunks):
            flowcfg = z.read(chunks[chunk])
    return res, description, flowcfg


class ResultFile(object):
    def __init__(self, filename):
        self._filename = filename
        self._data, self._description, self._flowcfg = \
            read_data_chunks(filename)
        if self._data is None:
            print("WARNING: File does not contain valid data")

        self._state, self._y1, self._y2 = False, False, False
        self.update_state(filename)

    def update_state(self, filename):
        try:
            self._state = read_state_chunks(filename)
            try:
                self._y2 = np.vstack(self._state["_y"])
                try:
                    self._y1 = np.vstack(self._state["y"])
                except Exception:
                    pass
            except Exception:
                pass
        except Exception:
            pass

    @property
    def filename(self):
        return self._filename

    @property
    def data(self):
        return self._data

    @property
    def description(self):
        return self._description

    @property
    def flowcfg(self):
        return self._flowcfg

    @property
    def state(self):
        if self._state is False:
            raise IndexError(
                "The file {} does not contain states".format(self._filename)
            )
        return self._state

    @property
    def y(self):
        if self._y1 is False:
            raise IndexError(
                "The file {} does not contain y".format(self._filename)
            )
        return self._y1

    @property
    def _y(self):
        if self._y2 is False:
            raise IndexError(
                "The file {} does not contain _y".format(self._filename)
            )
        return self._y2


if __name__ == "__main__":

    file1 = None
    if len(sys.argv) > 2:
        file1 = sys.argv[2]
    if len(sys.argv) > 1:
        file0 = sys.argv[1]
    else:
        print("Usage: loaddata.py FILE1 [FILE2]")
        sys.exit(0)

    dat = ResultFile(file0)

    print(dat.filename)
    print(dat.description)
    if dat.data is not None:
        print(dat.data.shape)
    try:
        print(dat.state.keys())
        print(dat._y.shape)
        print(dat.state["score"][-1])
        print(dat.y.shape)
    except Exception:
        pass

    if file1 is not None:
        dat1 = ResultFile(file1)
        print(dat1.filename)
        print(dat1.description)
        if dat1.data is not None:
            print(dat1.data.shape)
        try:
            print(dat1.state.keys())
            print(dat1.y.shape)
            print(dat1._y.shape)
            assert np.all(dat._y == dat1._y), "Should be same"
        except AssertionError as e:
            raise(e)
        except Exception:
            pass

        assert np.all(dat.description == dat1.description), "Should be same"
        assert np.all(dat.data == dat1.data), "Should be same"
