# Setup.py to build the cython extensions
# use with python ./setup.py build_ext --inplace <module_name>

from distutils.core import setup, Extension
from Cython.Build import cythonize
import numpy
import sys
import pathlib

module_src = None
module_name = None
for i, elem in enumerate(sys.argv):
    if elem.endswith(".pyx"):
        module_src = pathlib.Path(elem)
        module_name = module_src.stem
        break

if module_src is None:
    print("Set module pyx file")
    sys.exit(1)

del sys.argv[i]

setup(
    ext_modules=cythonize(
        Extension(
            module_name,
            sources=[module_src.as_posix()],
            include_dirs=[numpy.get_include()]
        )
    )
)
