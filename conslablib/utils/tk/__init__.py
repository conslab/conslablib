"""
Common TkInter utils like scrolling, dialog and tooltips
contains also a theme for linux (scidtheme)
"""

import tkinter as tk
from tkinter import ttk
import os
import platform
from pathlib import Path
import importlib.resources as resources

_OS = platform.system()

fsdialog_path = resources.path(
    "conslablib.utils.tk.linux.fsdialogs", "fsdialog_nomc.tcl"
)

def init_conslab_tk_style(root, color="blue", force=False):
    style = ttk.Style()
    if _OS == "Linux" or force:
        tcl = root.tk
        with fsdialog_path as path:
            tcl.eval(
                'source "'+ path.as_posix() + '"'
            )

        with resources.path(
            "conslablib.utils.tk.linux.theme", "scidthemes.tcl"
        ) as path:
            _load_scidtheme(
                root, style, path.as_posix(), color
            )
    return style


def _load_scidtheme(root, style, path, color="blue"):
    bg = root["bg"]
    check_bg = [bg[1:3], bg[3:5], bg[5:]]
    try:
        check_bg = [eval("0x{}".format(x)) for x in check_bg]
        if check_bg[0] < 128 or check_bg[1] < 128 or check_bg[2] < 128:
            raise Exception()
        else:
            check_bg = True
    except Exception:
        check_bg = False
    theme = ""
    with open(path) as of:
        for line in of:
            test = line.strip().split(" ")[0]
            if check_bg and (
                test.find("-frame") >= 0 or test.find("-pborder") >= 0
            ):
                rep = line.split(" ")
                rep[-1] = bg + "\n"
                line = " ".join(rep)
            theme += line
    cwd = os.getcwd()
    os.chdir(os.path.dirname(path))
    root.eval(theme)
    os.chdir(cwd)
    style.theme_use("scid"+color)


def get_curr_screen_size():
    root = tk.Tk()
    root.update_idletasks()
    root.attributes('-fullscreen', True)
    root.state('iconic')
    geometry = root.winfo_geometry()
    root.destroy()

    tmp1 = geometry.split("+")
    tmp2 = tmp1[0].split("x")

    return [int(tmp2[0]), int(tmp2[1]), int(tmp1[1]), int(tmp1[2])]


# ## TOOLTIP SUPPORT
# ## Implementation from https://stackoverflow.com/a/36221216
class ToolTip(object):
    """
    create a tooltip for a given widget
    """
    def __init__(self, widget, text='widget info'):
        self.waittime = 500     # miliseconds
        self.wraplength = 180   # pixels
        self.widget = widget
        self.text = text
        self.widget.bind("<Enter>", self.enter)
        self.widget.bind("<Leave>", self.leave)
        self.widget.bind("<ButtonPress>", self.leave)
        self.id = None
        self.tw = None

    def enter(self, event=None):
        self.schedule()

    def leave(self, event=None):
        self.unschedule()
        self.hidetip()

    def schedule(self):
        self.unschedule()
        self.id = self.widget.after(self.waittime, self.showtip)

    def unschedule(self):
        id = self.id
        self.id = None
        if id:
            self.widget.after_cancel(id)

    def showtip(self, event=None):
        x = y = 0
        x, y, cx, cy = self.widget.bbox("insert")
        x += self.widget.winfo_rootx() + 25
        y += self.widget.winfo_rooty() + 20
        # creates a toplevel window
        self.tw = tk.Toplevel(self.widget)
        # Leaves only the label and removes the app window
        self.tw.wm_overrideredirect(True)
        self.tw.wm_geometry("+%d+%d" % (x, y))
        label = tk.Label(
            self.tw, text=self.text, justify='left',
            background="#ffffff", relief='solid', borderwidth=1,
            wraplength=self.wraplength
        )
        label.pack(ipadx=1)

    def hidetip(self):
        tw = self.tw
        self.tw = None
        if tw:
            tw.destroy()


# ## DIALOGS
class Dialog(tk.Toplevel):

    def __init__(self, parent, title=None, **kwargs):

        super().__init__(parent)
        self.transient(parent)

        if title:
            self.title(title)

        self.parent = parent

        self.result = None

        btnBox = self.buttonbox()

        body = ttk.Frame(self)
        self.initial_focus = self.body(body, **kwargs)
        body.pack(padx=5, pady=5)

        btnBox.pack()

        self.grab_set()

        if not self.initial_focus:
            self.initial_focus = self

        self.protocol("WM_DELETE_WINDOW", self.cancel)

        self.geometry("+%d+%d" % (parent.winfo_rootx()+10,
                                  parent.winfo_rooty()+10))

        self.initial_focus.focus_set()

        self.wait_window(self)

    #
    # construction hooks

    def body(self, master, **kwargs):
        # create dialog body.  return widget that should have
        # initial focus.  this method should be overridden
        return self

    def buttonbox(self):
        # add standard button box. override if you don't want the
        # standard buttons

        box = ttk.Frame(self)

        # TODO localization?

        self.btnOK = ttk.Button(
            box, text="Save", width=10, command=self.ok, default=tk.ACTIVE
        )
        self.btnOK.pack(side=tk.LEFT, padx=5, pady=5)
        self.btnC = ttk.Button(
            box, text="Cancel", width=10, command=self.cancel
        )
        self.btnC.pack(side=tk.LEFT, padx=5, pady=5)

        self.bind("<Return>", self.ok)
        self.bind("<Escape>", self.cancel)

        return box

    #
    # standard button semantics

    def ok(self, event=None):

        if not self.validate():
            self.initial_focus.focus_set()  # put focus back
            return

        self.withdraw()
        self.update_idletasks()

        self.apply()

        self.cancel()

    def cancel(self, event=None):
        # put focus back to the parent window
        try:
            self.parent.focus_set()
        except Exception:
            pass
        self.destroy()

    #
    # command hooks

    def validate(self):
        return 1  # override

    def apply(self):
        pass  # override


# ## MOUSEWHEEL SUPPORT
class MousewheelSupport(object):
    # implemetation of singleton pattern
    _instance = None

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = object.__new__(cls)
        return cls._instance

    def __init__(self, root, horizontal_factor=2, vertical_factor=2):

        self._active_area = None

        if isinstance(horizontal_factor, int):
            self.horizontal_factor = horizontal_factor
        else:
            raise Exception("Vertical factor must be an integer.")

        if isinstance(vertical_factor, int):
            self.vertical_factor = vertical_factor
        else:
            raise Exception("Horizontal factor must be an integer.")

        if _OS == "Linux":
            root.bind_all('<4>', self._on_mousewheel,  add='+')
            root.bind_all('<5>', self._on_mousewheel,  add='+')
        else:
            # Windows and MacOS
            root.bind_all("<MouseWheel>", self._on_mousewheel,  add='+')

    def _on_mousewheel(self, event):
        if self._active_area:
            self._active_area.onMouseWheel(event)

    def _mousewheel_bind(self, widget):
        self._active_area = widget

    def _mousewheel_unbind(self):
        self._active_area = None

    def add_support_to(
        self, widget=None, xscrollbar=None, yscrollbar=None,
        what="units", horizontal_factor=None, vertical_factor=None
    ):
        if xscrollbar is None and yscrollbar is None:
            return

        if xscrollbar is not None:
            horizontal_factor = horizontal_factor or self.horizontal_factor

            xscrollbar.onMouseWheel = self._make_mouse_wheel_handler(
                widget, 'x', self.horizontal_factor, what
            )
            xscrollbar.bind(
                '<Enter>',
                lambda event,
                scrollbar=xscrollbar: self._mousewheel_bind(scrollbar)
            )
            xscrollbar.bind('<Leave>', lambda event: self._mousewheel_unbind())

        if yscrollbar is not None:
            vertical_factor = vertical_factor or self.vertical_factor

            yscrollbar.onMouseWheel = self._make_mouse_wheel_handler(
                widget, 'y', self.vertical_factor, what
            )
            yscrollbar.bind(
                '<Enter>',
                lambda event,
                scrollbar=yscrollbar: self._mousewheel_bind(scrollbar)
            )
            yscrollbar.bind('<Leave>', lambda event: self._mousewheel_unbind())

        main_scrollbar = yscrollbar if yscrollbar is not None else xscrollbar

        if widget is not None:
            if isinstance(widget, list) or isinstance(widget, tuple):
                list_of_widgets = widget
                for widget in list_of_widgets:
                    widget.bind(
                        '<Enter>',
                        lambda event: self._mousewheel_bind(widget)
                    )
                    widget.bind(
                        '<Leave>',
                        lambda event: self._mousewheel_unbind()
                    )

                    widget.onMouseWheel = main_scrollbar.onMouseWheel
            else:
                widget.bind(
                    '<Enter>',
                    lambda event: self._mousewheel_bind(widget)
                )
                widget.bind(
                    '<Leave>',
                    lambda event: self._mousewheel_unbind()
                )

                widget.onMouseWheel = main_scrollbar.onMouseWheel

    @staticmethod
    def _make_mouse_wheel_handler(widget, orient, factor=1, what="units"):
        view_command = getattr(widget, orient+'view')

        if _OS == 'Linux':
            def onMouseWheel(event):
                if event.num == 4:
                    view_command("scroll", (-1)*factor, what)
                elif event.num == 5:
                    view_command("scroll", factor, what)

        elif _OS == 'Windows':
            def onMouseWheel(event):
                view_command(
                    "scroll", (-1)*int((event.delta/120)*factor), what
                )

        elif _OS == 'Darwin':
            def onMouseWheel(event):
                view_command("scroll", event.delta, what)

        return onMouseWheel


# ## SCROLLED TEXT FRAME
class ScrolledTextFrame(ttk.Frame):
    def __init__(self, root, **kwargs):
        super().__init__(root)

        self.vbar = ttk.Scrollbar(self, orient="vertical")
        self.text = tk.Text(self, yscrollcommand=self.vbar.set, **kwargs)
        self.vbar.config(command=self.text.yview)

        self.vbar.pack(side=tk.RIGHT, fill=tk.Y)
        self.text.pack(side=tk.LEFT, fill=tk.BOTH, expand=tk.TRUE)


# Idea from https://stackoverflow.com/a/13169685
class ToggledFrame(ttk.Frame):

    def __init__(
        self, parent, text="", btn_txt=("\u25B6", "\u25BC"), *args, **options
    ):
        super().__init__(parent, *args, **options)
        
        self.CLOSED = btn_txt[0]
        self.OPENED = btn_txt[1]

        s = ttk.Style()
        s.configure('toggle.TLabel', background="darkgray")
        s.configure('toggle.TSeparator', background="darkgray")
        s.configure('toggle.TFrame', background="darkgray")

        self.title_frame = ttk.Frame(self)
        self.title_frame.pack(fill=tk.X, expand=tk.TRUE)
        self.title_frame.bind("<Enter>", self.on_enter)
        self.title_frame.bind("<Leave>", self.on_leave)

        self.label = ttk.Label(self.title_frame, text=text)
        self.label.pack(side=tk.LEFT, fill=tk.X)

        self.sep = ttk.Separator(self.title_frame, orient="horizontal")
        self.sep.pack(side=tk.LEFT, fill=tk.X, expand=tk.TRUE, padx=1)

        self.toggle_label = ttk.Label(
            self.title_frame, text=self.CLOSED
        )
        self.toggle_label.pack(side=tk.LEFT)
        
        self.bind_head("<Button-1>", self.toggle)
        
        self.sub_frame = ttk.Frame(self)
        
    def bind_head(self, event, callback):
        self.title_frame.bind(event, callback)
        self.toggle_label.bind(event, callback)
        self.label.bind(event, callback)
        self.sep.bind(event, callback)
        
    def on_enter(self, event):
        self.label.configure(style="toggle.TLabel")
        self.toggle_label.configure(style="toggle.TLabel")
        self.sep.configure(style="toggle.TSeparator")
        self.title_frame.configure(style="toggle.TFrame")

    def on_leave(self, event):
        self.label.configure(style="TLabel")
        self.toggle_label.configure(style="TLabel")
        self.sep.configure(style="TSeparator")
        self.title_frame.configure(style="TFrame")

    def toggle(self, event):
        if self.toggle_label.cget("text") == self.CLOSED:
            self.sub_frame.pack(fill=tk.X, expand=tk.TRUE)
            self.toggle_label.configure(text=self.OPENED)
        else:
            self.sub_frame.forget()
            self.toggle_label.configure(text=self.CLOSED)


# Taken from https://stackoverflow.com/a/39459376
class ClosableNotebook(ttk.Notebook):
    """A ttk Notebook with close buttons on each tab"""

    __initialized = False
    __images = ()

    def __init__(self, *args, **kwargs):
        if not self.__initialized:
            ClosableNotebook.__initialize_custom_style()
            ClosableNotebook.__inititialized = True

        kwargs["style"] = "ClosableNotebook"
        ttk.Notebook.__init__(self, *args, **kwargs)

        self._active = None

        self.bind("<ButtonPress-1>", self.on_close_press, True)
        self.bind("<ButtonRelease-1>", self.on_close_release)

    def on_close_press(self, event):
        """Called when the button is pressed over the close button"""

        element = self.identify(event.x, event.y)

        if "close" in element:
            index = self.index("@%d,%d" % (event.x, event.y))
            self.state(['pressed'])
            self._active = index

    def on_close_release(self, event):
        """Called when the button is released over the close button"""
        if not self.instate(['pressed']):
            return

        element = self.identify(event.x, event.y)
        index = self.index("@%d,%d" % (event.x, event.y))

        if "close" in element and self._active == index:
            self.forget(index)
            self.event_generate("<<NotebookTabClosed>>", state=index)

        self.state(["!pressed"])
        self._active = None

    @classmethod
    def __initialize_custom_style(cls):
        style = ttk.Style()
        cls.__images = (
            tk.PhotoImage("img_close", data='''
                R0lGODlhCAAIAMIBAAAAADs7O4+Pj9nZ2Ts7Ozs7Ozs7Ozs7OyH+EUNyZWF0ZWQg
                d2l0aCBHSU1QACH5BAEKAAQALAAAAAAIAAgAAAMVGDBEA0qNJyGw7AmxmuaZhWEU
                5kEJADs=
                '''),
            tk.PhotoImage("img_closeactive", data='''
                R0lGODlhCAAIAMIEAAAAAP/SAP/bNNnZ2cbGxsbGxsbGxsbGxiH5BAEKAAQALAAA
                AAAIAAgAAAMVGDBEA0qNJyGw7AmxmuaZhWEU5kEJADs=
                '''),
            tk.PhotoImage("img_closepressed", data='''
                R0lGODlhCAAIAMIEAAAAAOUqKv9mZtnZ2Ts7Ozs7Ozs7Ozs7OyH+EUNyZWF0ZWQg
                d2l0aCBHSU1QACH5BAEKAAQALAAAAAAIAAgAAAMVGDBEA0qNJyGw7AmxmuaZhWEU
                5kEJADs=
            ''')
        )

        style.element_create(
            "close", "image", "img_close", (
                "active", "pressed", "!disabled", "img_closepressed"
            ), ("active", "!disabled", "img_closeactive"),
            border=8, sticky=''
        )
        style.layout("ClosableNotebook", [
            ("ClosableNotebook.client", {"sticky": "nswe"})
        ])
        style.layout("ClosableNotebook.Tab", [
            ("ClosableNotebook.tab", {
                "sticky": "nswe",
                "children": [
                    ("ClosableNotebook.padding", {
                        "side": "top",
                        "sticky": "nswe",
                        "children": [
                            ("ClosableNotebook.focus", {
                                "side": "top",
                                "sticky": "nswe",
                                "children": [
                                    ("ClosableNotebook.label", {
                                        "side": "left", "sticky": ''
                                    }),
                                    ("ClosableNotebook.close", {
                                        "side": "left", "sticky": ''
                                    }),
                                ]
                            })
                        ]
                    })
                ]
            })
        ])


if __name__ == "__main__":
    def check(event):
        event.widget.winfo_children()[event.state].destroy()
        print(event.widget.winfo_children())

    root = tk.Tk()
    init_conslab_tk_style(root, force=True)
    nb = ClosableNotebook(root)
    nb.add(tk.Frame(nb), text="Hello")
    nb.add(tk.Frame(nb), text="Hello2")
    nb.pack()
    ttk.Button(root, text="Hallo Welt").pack()
    nb.bind("<<NotebookTabClosed>>", check)
    try:
        tk.mainloop()
    except KeyboardInterrupt:
        pass
