import tkinter.filedialog as fd
from tkinter.messagebox import askyesno
import os
from . import fsdialog_path

def askopenfilename(parent, title, initialdir=None, filetypes=[], load=False):
    tcl = parent.tk
    init = ""
    if initialdir:
        init = " -initialdir " + initialdir

    types = ""
    for msg, t in filetypes:
        if len(t) > 1 and t[0] == "*":
            t = t[1:]
        types += " {{"+msg+"} {"+t+"} }"
    try:
        if load:
            with fsdialog_path as path:
                tcl.eval(
                    "source " + path.as_posix()
                )
        res = tcl.eval(
            "ttk::getOpenFile -filetypes {" + types + "} -title {" +
            title + "} -parent " + parent._w + init
        )
    except Exception:
        res = fd.askopenfilename(
            parent=parent,
            title=title,
            filetypes=filetypes,
            initialdir=initialdir
        )
    return res

def askopenfile(parent, title, initialdir=None, filetypes=[], load=False):
    import warnings
    warnings.warn("askopenfile is deprecated from this module use askopenfilename instead.", DeprecationWarning)
    return askopenfilename(parent, title, initialdir, filetypes, load)

def asksaveasfilename(
    parent, title, initialfile=None, initialdir=None,
    filetypes=[], load=False
):
    tcl = parent.tk
    initd = ""
    if initialdir:
        initd = " -initialdir " + initialdir

    initf = ""
    if initialfile:
        initf = " -initialfile " + initialfile

    types = ""
    for msg, t in filetypes:
        if len(t) > 1 and t[0] == "*":
            t = t[1:]
        types += " {{"+msg+"} {"+t+"} }"
    try:
        if load:
            with fsdialog_path as path:
                tcl.eval(
                    "source " + path.as_posix()
                )
        res = tcl.eval(
            "ttk::getSaveFile -filetypes {" + types + "} -title {" +
            title + "} -parent " + parent._w + initf + initd +
            " -confirmoverwrite 0"
        )
        # On linux the overwrite confirmation was not shown, so it is created
        if os.path.exists(res):
            ok = askyesno(
                title="Overwrite confirmation",
                message="Do you want to overwrite\n\n{}?".format(
                    os.path.basename(res)
                ))
            if not ok:
                res = ""
    except Exception:
        res = fd.asksaveasfilename(
            parent=parent,
            title=title,
            filetypes=filetypes,
            initialdir=initialdir,
            initialfile=initialfile
        )
    return res


def askdirectory(parent, title, initialdir=None, load=False):
    tcl = parent.tk
    init = ""
    if initialdir:
        init = " -initialdir " + initialdir
    try:
        if load:
            with fsdialog_path as path:
                tcl.eval(
                    "source " + path.as_posix()
                )
        res = tcl.eval(
            "ttk::chooseDirectory -title {" + title +
            "} -parent " + parent._w + init
        )
    except Exception:
        res = fd.askdirectory(
            parent=parent, title=title, initialdir=initialdir
        )
    return res
